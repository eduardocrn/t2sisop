#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 

/* Exemplo de servidor com sockets TCP que retorna data e hora do sistema. */

int filesize(FILE* file){
	int curr = ftell(file);
	int sz;
	fseek(file, 0L, SEEK_END);
	sz = ftell(file);
	fseek(file, 0L, SEEK_SET);
	return sz;
}

int main(int argc, char *argv[])
{
    int listenfd = 0, connfd = 0, n = 0;
    struct sockaddr_in serv_addr, clientaddr;
    char sendBuff[1025];
    char fileSizeBuff[10];
    char fileName[20];
    time_t ticks; 
    socklen_t sz;
    FILE* fp;	

    memset(sendBuff, 0, sizeof(sendBuff)); 

    /* Passo 1 - Criar Socket */
    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
	  perror("socket: ");
	  exit(1);
    }

    /* Passo 2 - Configurar estrutura sockaddr_in */
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(4000); 
    
    /* Passo 3 - Associar socket com a estrutura sockaddr_in     */
    if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
   	  perror("bind: ");
	  exit(1);
    }

    /* Passo 4 - Tornar o servidor ativo  */
    listen(listenfd, 10); 

    while(1)
    {
	/* Passo 5 - Aguardar conexão do cliente.  */
        if ((connfd = accept(listenfd, (struct sockaddr*)&clientaddr, &sz)) < 0){
		perror("accept: ");
		continue;
	}
	
	memset(fileName, 0, sizeof(fileName));
        memset(fileSizeBuff, 0, sizeof(fileSizeBuff));

	/* Recebe nome do arquivo */
	n = recv(connfd, fileName, sizeof(fileName)-1, 0);
	printf("\n Nome recebido: %s", fileName);
	
	/* Le tamanho do arquivo e envia */
	fp = fopen(fileName,"r");
	snprintf(fileSizeBuff, sizeof(fileSizeBuff), "%d", filesize(fp));
	send(connfd, fileSizeBuff, strlen(fileSizeBuff), 0);


		
	/* Encerra conexão com o cliente. */
        close(connfd);
     }
}


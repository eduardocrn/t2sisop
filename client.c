#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

int main(int argc, char *argv[])
{
    int sockfd = 0, n = 0;
    char recvBuff[1024];
    struct sockaddr_in serv_addr; 

    if(argc != 4)
    {
        printf("\n Usage: %s <ip of server> <port>\n",argv[0]);
        return 1;
    } 

    memset(recvBuff, 0,sizeof(recvBuff));
    
    /* Passo 1 - Criar socket */
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("socket: ");
        exit(1);
    } 

    /* Passo 2 - Configura struct sockaddr_in */
    memset(&serv_addr, 0, sizeof(serv_addr)); 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2]));
 
    /* converte Ip em formato string para o formato exigido pela struct sockaddr_in*/
    if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        exit(1);
    } 

    /* Passo 3 - Conectar ao servidor */
    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       perror("connect: ");
       exit(1);
    } 

	
    
    /* Envia nome do arquivo */
    char fileName[20];
    memset(fileName, 0, sizeof(fileName));
    snprintf(fileName, sizeof(fileName), "%.24s\r\n", argv[3]);
    send(sockfd, &fileName, strlen(fileName), 0);
	
    /* Recebe o tamanho do arquivo */
    char fileSize[10];
    memset(fileSize, 0, sizeof(fileSize));
    n = recv(sockfd, fileSize, sizeof(fileSize)-1, 0);
    printf("\n Tamanho do arquivo: %s", fileSize);
	
    /* Recebe arquivo */
    while((n = recv(sockfd, recvBuff, sizeof(recvBuff), 0))
    {
		
    }
	
    close(sockfd);

    return 0;
}

